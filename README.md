# Info

General information about the use-case implemented in the different languages/frameworks

## Use Case

Supose you have an application/website that Heroes use as a registry where they can specify their skills and can commerce with gadgets and stuff (lets hope no villan hacks this).

The idea is to build such application having a micro-service aproach where every main concern will have it's own service such as a Hero Service, a Commerce Service and a Security Service

### Hero Service

Main repository for hero and their skills so a hero may have multiple skills and a skill can be assigned to multiple heroes (Hero/Skill CRUD)

### Commerce Service

Should be an hero independent platform where you can commerce items between users so a user publish a product and another user buys it

### Security Service

Should be where all users are registered and handle all the authentication/authorization part of the application. All heroes are users but not all users are heroes


## Front End

```mermaid
graph TD;
    A(Login) --> H(Security Endpoint);
    B(Register) --> H;
    C(Dashboard) --> I(Hero Endpoint);
    D(Create/Update Hero) --> I;
    E(Create/update Skills) --> I;
    F(Sell Items) --> J(Commerce Endpoint);
    G(Purchase Items) --> J;
```

## Back End

```mermaid
graph TD;
    A(Front End) --> B(Reverse Proxy);
    B --> C(Security Service);
    B --> D(Hero Service);
    B --> E(Commerce Service);
    C --> F[Users DB];
    D --> G[Heroes DB];
    E --> H[Items DB];
```
